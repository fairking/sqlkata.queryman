﻿using SqlKata;
using SqlKata.Compilers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace QueryMan
{
    public class QueryBuilder : BaseQueryBuilder<Query, QueryBuilder>
    {
        protected Query _query => base._baseQuery as Query ?? throw new ArgumentNullException(nameof(_query));

        private readonly QueryRunner _runner;

        public QueryRunner Runner => _runner ?? throw new Exception("QueryRunner does not exist in the current QueryBuilder. Use QueryBuilder(QueryRunner queryRunner) constructor intead.");

        private readonly IDictionary<string, string> _selects = new Dictionary<string, string>(); // <alias, column>
        private readonly IDictionary<string, QueryBuilder> _selectQueries = new Dictionary<string, QueryBuilder>(); // <alias, query>
        private readonly IDictionary<string, string> _selectsRaw = new Dictionary<string, string>(); // <alias, raw_query>
        private readonly IDictionary<string, string> _selectAggrs = new Dictionary<string, string>(); // <alias, aggregation>

        public QueryBuilder(Compiler compiler, bool snakeCase = false, Query query = null) 
            : base(query ?? new Query(), compiler, snakeCase)
        {
        }

        public QueryBuilder(QueryRunner queryRunner, bool snakeCase = false, Query query = null) 
            : base(
                  query ?? queryRunner?.Query() ?? throw new ArgumentNullException(nameof(queryRunner)), 
                  queryRunner?.Factory?.Compiler ?? throw new ArgumentNullException(nameof(queryRunner)),
                  snakeCase
            )
        {
            _runner = queryRunner ?? throw new ArgumentNullException(nameof(queryRunner));
        }

        #region Clone

        public override QueryBuilder CreateNew()
        {
            return _runner != null
                ? new QueryBuilder(_runner, _snakeCase)
                : new QueryBuilder(_compiler, _snakeCase);
        }

        public override QueryBuilder CreateClone()
        {
            return _runner != null
                ? new QueryBuilder(_runner, _snakeCase, _query.Clone())
                : new QueryBuilder(_compiler, _snakeCase, _query.Clone());
        }

        #endregion Clone

        #region Selects

        public void Select(string column, string alias)
        {
            _selects.Add(alias, column);
            _query.Select($"{column} AS {alias}");
        }

        public void SelectQuery(QueryBuilder query, string alias)
        {
            _selectQueries.Add(alias, query);
            _query.Select(query.ToQuery(), alias);
        }

        public void SelectRaw(string query, string alias)
        {
            _selectsRaw.Add(alias, query);
            _query.SelectRaw($"{query} AS {alias}");
        }

        public void SelectRaw<A>(string query, Expression<Func<A>> alias)
        {
            var aliasName = GetAliasName(alias);
            _selectsRaw.Add(aliasName, query);
            _query.SelectRaw($"{query} AS {aliasName}");
        }

        public QueryBuilder Select<T, A>(Expression<Func<T>> column, Expression<Func<A>> alias)
        {
            Select($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", GetAliasName(alias));
            return this;
        }

        public QueryBuilder SelectAll<T>()
        {
            From<T>();

            foreach (var col in QueryHelper.GetColumns<T>(_snakeCase))
            {
                SelectRaw(col.Value, col.Key);
            }

            return this;
        }

        public QueryBuilder SelectAll<T>(Expression<Func<T>> alias)
        {
            From<T>(alias);

            foreach (var col in QueryHelper.GetColumns<T>(_snakeCase))
            {
                SelectRaw(col.Value, col.Key);
            }

            return this;
        }

        #endregion Selects

        #region Pagination

        /// <summary>
        /// Skips the number of rows, similar to OFFSET
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public QueryBuilder Skip(int offset)
        {
            _query.Skip(offset);
            return this;
        }

        /// <summary>
        /// Takes the number of rows similar to TOP or LIMIT.
        /// </summary>
        public QueryBuilder Take(int limit)
        {
            _query.Take(limit);
            return this;
        }

        /// <summary>
        /// Gets the limited number of rows per page
        /// </summary>
        /// <param name="page">Page number starts from 1</param>
        /// <param name="perPage">Number of rows per page</param>
        /// <returns></returns>
        public QueryBuilder ForPage(int page, int perPage)
        {
            if (page <= 0)
                throw new ArgumentException("Must be greater than zero.", nameof(page));

            if (perPage <= 0)
                throw new ArgumentException("Must be greater than zero.", nameof(perPage));

            _query.ForPage(page, perPage);
            return this;
        }

        #endregion Pagination

        #region Join

        public QueryBuilder Join<T1, T2, T3>(Expression<Func<T1>> alias, Expression<Func<T2>> firstColumn, Expression<Func<T3>> secondColumn, string op = "=")
        {
            _query.Join(
                $"{GetTableName<T1>()} AS {GetAliasName(alias)}",
                $"{GetAliasNameFromPropery(firstColumn)}.{GetPropertyName(firstColumn)}",
                $"{GetAliasNameFromPropery(secondColumn)}.{GetPropertyName(secondColumn)}",
                op: op
            );
            return this;
        }

        public QueryBuilder Join<T>(Expression<Func<T>> alias, Func<JoinBuilder, JoinBuilder> joinQuery)
        {
            _query.Join(
                $"{GetTableName<T>()} AS {GetAliasName(alias)}",
                join => joinQuery.Invoke(new JoinBuilder(_compiler, _snakeCase)).ToQuery()
            );
            return this;
        }

        public QueryBuilder LeftJoin<T1, T2, T3>(Expression<Func<T1>> alias, Expression<Func<T2>> firstColumn, Expression<Func<T3>> secondColumn, string op = "=")
        {
            _query.LeftJoin(
                $"{GetTableName<T1>()} AS {GetAliasName(alias)}",
                $"{GetAliasNameFromPropery(firstColumn)}.{GetPropertyName(firstColumn)}",
                $"{GetAliasNameFromPropery(secondColumn)}.{GetPropertyName(secondColumn)}",
                op: op
            );
            return this;
        }

        public QueryBuilder LeftJoin<T>(Expression<Func<T>> alias, Func<JoinBuilder, JoinBuilder> joinQuery)
        {
            _query.LeftJoin(
                $"{GetTableName<T>()} AS {GetAliasName(alias)}",
                join => joinQuery.Invoke(new JoinBuilder(_compiler, _snakeCase)).ToQuery()
            );
            return this;
        }

        public QueryBuilder RightJoin<T1, T2, T3>(Expression<Func<T1>> alias, Expression<Func<T2>> firstColumn, Expression<Func<T3>> secondColumn, string op = "=")
        {
            _query.RightJoin(
                $"{GetTableName<T1>()} AS {GetAliasName(alias)}",
                $"{GetAliasNameFromPropery(firstColumn)}.{GetPropertyName(firstColumn)}",
                $"{GetAliasNameFromPropery(secondColumn)}.{GetPropertyName(secondColumn)}",
                op: op
            );
            return this;
        }

        public QueryBuilder RightJoin<T>(Expression<Func<T>> alias, Func<JoinBuilder, JoinBuilder> joinQuery)
        {
            _query.RightJoin(
                $"{GetTableName<T>()} AS {GetAliasName(alias)}",
                join => joinQuery.Invoke(new JoinBuilder(_compiler, _snakeCase)).ToQuery()
            );
            return this;
        }

        public QueryBuilder CrossJoin<T>(Expression<Func<T>> alias)
        {
            _query.CrossJoin(GetTableName<T>());
            return this;
        }

        #endregion Join

        #region Orders

        public QueryBuilder OrderBy<T>(Expression<Func<T>> column)
        {
            _query.OrderBy($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return this;
        }

        public QueryBuilder OrderByDesc<T>(Expression<Func<T>> column)
        {
            _query.OrderByDesc($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return this;
        }

        public QueryBuilder OrderByRaw<T>(string orderQuery)
        {
            _query.OrderByRaw(orderQuery);
            return this;
        }

        #endregion Orders

        #region Aggregations

        public void SelectAggr(string query, string alias)
        {
            _selectAggrs.Add(alias, query);
            _query.SelectRaw($"{query} AS {alias}");
        }

        public void SelectAggr<A>(string query, Expression<Func<A>> alias)
        {
            var aliasName = GetAliasName(alias);
            _selectAggrs.Add(aliasName, query);
            _query.SelectRaw($"{query} AS {aliasName}");
        }

        public QueryBuilder SelectCount()
        {
            _query.AsCount();
            return this;
        }

        public QueryBuilder SelectCount<T, A>(Expression<Func<T>> column, Expression<Func<A>> alias)
        {
            SelectAggr($"COUNT({GetAliasNameFromPropery(column)}.{GetPropertyName(column)})", GetAliasName(alias));
            return this;
        }

        public QueryBuilder SelectMin<T, A>(Expression<Func<T>> column, Expression<Func<A>> alias)
        {
            SelectAggr($"MIN({GetAliasNameFromPropery(column)}.{GetPropertyName(column)})", GetAliasName(alias));
            return this;
        }

        public QueryBuilder SelectMax<T, A>(Expression<Func<T>> column, Expression<Func<A>> alias)
        {
            SelectAggr($"MIN({GetAliasNameFromPropery(column)}.{GetPropertyName(column)})", GetAliasName(alias));
            return this;
        }

        public QueryBuilder SelectAvg<T, A>(Expression<Func<T>> column, Expression<Func<A>> alias)
        {
            SelectAggr($"AVG({GetAliasNameFromPropery(column)}.{GetPropertyName(column)})", GetAliasName(alias));
            return this;
        }

        public QueryBuilder SelectSum<T, A>(Expression<Func<T>> column, Expression<Func<A>> alias)
        {
            SelectAggr($"SUM({GetAliasNameFromPropery(column)}.{GetPropertyName(column)})", GetAliasName(alias));
            return this;
        }

        public QueryBuilder GroupBy()
        {
            foreach (var select in _selects)
                _query.GroupBy(select.Value);

            _selects.Clear();

            foreach (var selectRaw in _selectsRaw)
                _query.GroupByRaw(selectRaw.Value);

            _selectsRaw.Clear();

            foreach (var selectQuery in _selectQueries)
                _query.GroupByRaw(_compiler.Compile(selectQuery.Value.ToQuery()).ToString());

            _selectQueries.Clear();

            _selectAggrs.Clear();

            return this;
        }

        #endregion Aggregations

        #region Private Methods

        #endregion Private Methods
    }
}
