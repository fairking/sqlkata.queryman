﻿using SqlKata;
using SqlKata.Compilers;
using System;

namespace QueryMan
{
    public class JoinBuilder : BaseQueryBuilder<Join, JoinBuilder>
    {
        protected Join _join => base._baseQuery as Join ?? throw new ArgumentNullException(nameof(_join));

        public JoinBuilder(Compiler compiler, bool snakeCase = false, Join join = null) 
            : base(join ?? new Join(), compiler, snakeCase)
        {

        }

        #region Clone

        public override JoinBuilder CreateNew()
        {
            return new JoinBuilder(_compiler, _snakeCase);
        }

        public override JoinBuilder CreateClone()
        {
            return new JoinBuilder(_compiler, _snakeCase, _join.Clone());
        }

        #endregion Clone

    }
}
