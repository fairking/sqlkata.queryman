﻿using System;
using System.Collections.Generic;

namespace QueryMan.Helpers
{
    public static class RandomStringGenerator
	{
		private static object objLock = new object();

		private static char[] characters = {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
			'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
			'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z'
		};

		/// <summary>
		/// Creates a new Unique Identity HashCode (length 16 chars)
		/// </summary>
		/// <returns></returns>
		public static string WebHash(Guid fromGuid = default(Guid))
		{
			lock (objLock)
				return RandomString(16, (fromGuid != default(Guid) ? fromGuid.ToByteArray() : null));
		}

		public static string RandomString(int length, byte[] customBytes = null, bool includeCapitals = false)
		{
			Stack<byte> bytes = customBytes != null ? new Stack<byte>(customBytes) : new Stack<byte>();
			string output = string.Empty;

			for (int i = 0; i < length; i++)
			{
				if (bytes.Count == 0)
					bytes = new Stack<byte>(Guid.NewGuid().ToByteArray());
				byte pop = bytes.Pop();
				output += characters[pop % (includeCapitals ? 62 : 36)];
			}
			return output;
		}

		private static readonly Random _random = new Random();

		public static int RandomInt(int? maxValue = null)
		{
			lock (_random)
			{
				return _random.Next(maxValue ?? int.MaxValue);
			};
		}
	}
}
