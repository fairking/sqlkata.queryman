﻿using System;

namespace QueryMan.Helpers
{
    public static class ReflectionHelper
    {
        public static object GetDefaultValue(this Type t)
        {
            if (t.IsValueType)
                return Activator.CreateInstance(t);

            return null;
        }
    }
}
