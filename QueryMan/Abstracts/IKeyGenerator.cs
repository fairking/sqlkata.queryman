﻿namespace QueryMan.Abstracts
{
    public interface IKeyGenerator
    {
        object Generate();
    }
}
