﻿using CaseExtensions;
using QueryMan.Exceptions;
using QueryMan.Helpers;
using QueryMan.Observable;
using SqlKata;
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace QueryMan
{
    public class QueryRunner : IDisposable
    {
        private readonly QueryFactory _factory;
        private IDbTransaction _transaction;
        private int _timeout;
        private readonly IsolationLevel _defaultIsoLevel;
        private bool _snakeCase;

        public QueryFactory Factory => _factory;

        public QueryRunner(IDbConnection connection, Compiler compiler, bool snakeCase = false, int timeout = 30, IsolationLevel defaultIsoLevel = IsolationLevel.ReadCommitted)
        {
            _snakeCase = snakeCase;

            if (timeout <= 0)
                throw new ArgumentException("Value must be greater than zero.", nameof(timeout));

            _timeout = timeout;

            _factory = new QueryFactory(
                connection ?? throw new ArgumentNullException(nameof(connection)),
                compiler ?? throw new ArgumentNullException(nameof(compiler)),
                timeout
            );

            _defaultIsoLevel = defaultIsoLevel;
        }

        public void BeginTransaction(IsolationLevel? isoLevel = null)
        {
            if (_transaction == null)
            {
                if (_factory.Connection.State != ConnectionState.Open)
                    _factory.Connection.Open();
                _transaction = _factory.Connection.BeginTransaction(isoLevel ?? _defaultIsoLevel);
            }
            else
            {
                throw new Exception("The transaction already exists.");
            }
        }

        public void Commit()
        {
            if (_transaction == null)
                throw new Exception("Database transaction does not exist.");

            _transaction.Commit();
            _transaction.Dispose();
            _transaction = null;
        }

        public void Rollback()
        {
            if (_transaction == null)
                throw new Exception("Database transaction does not exist.");

            _transaction.Rollback();
            _transaction.Dispose();
            _transaction = null;
        }

        #region Selects

        public Query Query()
        {
            return _factory.Query();
        }

        internal QueryBuilder Query<T>()
        {
            return new QueryBuilder(this, _snakeCase).From<T>();
        }

        public QueryBuilder Query<T>(Expression<Func<T>> alias)
        {
            return new QueryBuilder(this, _snakeCase).From(alias);
        }

        public QueryBuilder SelectAll<T>()
        {
            return new QueryBuilder(this, _snakeCase).SelectAll<T>();
        }

        public QueryBuilder SelectAll<T>(Expression<Func<T>> alias)
        {
            return new QueryBuilder(this, _snakeCase).SelectAll<T>(alias);
        }

        #endregion Selects

        #region List Results

        public IList<T> ToList<T>(QueryBuilder query) where T : class
        {
            return ToList<T>(query.ToQuery());
        }

        public IList<T> ToList<T>(Query query) where T : class
        {
            var result = ToListNoProxy<T>(query);

            // Create a proxy
            result = result.Select(x => ObservableProxy.Create(x)).ToList();

            return result;
        }

        public async Task<IList<T>> ToListAsync<T>(QueryBuilder query, CancellationToken cancellationToken = default) where T : class
        {
            return await ToListAsync<T>(query.ToQuery(), cancellationToken);
        }

        public async Task<IList<T>> ToListAsync<T>(Query query, CancellationToken cancellationToken = default) where T : class
        {
            var result = await ToListNoProxyAsync<T>(query, cancellationToken);

            // Create a proxy
            result = result.Select(x => ObservableProxy.Create(x)).ToList();

            return result;
        }

        public IList<T> ToListNoProxy<T>(QueryBuilder query)
        {
            return ToListNoProxy<T>(query.ToQuery());
        }

        public IList<T> ToListNoProxy<T>(Query query)
        {
            var result = query.Get<T>(_transaction, _timeout);

            return result.ToList();
        }

        public async Task<IList<T>> ToListNoProxyAsync<T>(QueryBuilder query, CancellationToken cancellationToken = default)
        {
            return await ToListNoProxyAsync<T>(query.ToQuery(), cancellationToken);
        }

        public async Task<IList<T>> ToListNoProxyAsync<T>(Query query, CancellationToken cancellationToken = default)
        {
            var result = await query.GetAsync<T>(_transaction, _timeout, cancellationToken);

            return result.ToList();
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public PaginationResult<T> ToPaginated<T>(QueryBuilder query, int page, int perPage) where T : class
        {
            return ToPaginated<T>(query.ToQuery(), page, perPage);
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public PaginationResult<T> ToPaginated<T>(Query query, int page, int perPage) where T : class
        {
            var result = ToPaginatedNoProxy<T>(query, page, perPage);

            // Create a proxy
            result.List = result.List.Select(x => ObservableProxy.Create(x)).ToList();

            return result;
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PaginationResult<T>> ToPaginatedAsync<T>(QueryBuilder query, int page, int perPage, CancellationToken cancellationToken = default) where T : class
        {
            return await ToPaginatedAsync<T>(query.ToQuery(), page, perPage, cancellationToken);
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<PaginationResult<T>> ToPaginatedAsync<T>(Query query, int page, int perPage, CancellationToken cancellationToken = default) where T : class
        {
            var result = await ToPaginatedNoProxyAsync<T>(query, page, perPage, cancellationToken);

            // Create a proxy
            result.List = result.List.Select(x => ObservableProxy.Create(x)).ToList();

            return result;
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public PaginationResult<T> ToPaginatedNoProxy<T>(QueryBuilder query, int page, int perPage) where T : class
        {
            return ToPaginatedNoProxy<T>(query.ToQuery(), page, perPage);
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public PaginationResult<T> ToPaginatedNoProxy<T>(Query query, int page, int perPage) where T : class
        {
            if (page <= 0)
                throw new ArgumentException("Must be greater than zero.", nameof(page));

            if (perPage <= 0)
                throw new ArgumentException("Must be greater than zero.", nameof(perPage));

            var result = query.Paginate<T>(page, perPage, _transaction, _timeout);
            return result;
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PaginationResult<T>> ToPaginatedNoProxyAsync<T>(QueryBuilder query, int page, int perPage, CancellationToken cancellationToken = default) where T : class
        {
            return await ToPaginatedNoProxyAsync<T>(query.ToQuery(), page, perPage, cancellationToken);
        }

        /// <summary>
        /// Gives a paginated result
        /// </summary>
        /// <typeparam name="T">The type of returned entity.</typeparam>
        /// <param name="query"></param>
        /// <param name="page">Starts from 1</param>
        /// <param name="perPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<PaginationResult<T>> ToPaginatedNoProxyAsync<T>(Query query, int page, int perPage, CancellationToken cancellationToken = default) where T : class
        {
            if (page <= 0)
                throw new ArgumentException("Must be greater than zero.", nameof(page));

            if (perPage <= 0)
                throw new ArgumentException("Must be greater than zero.", nameof(perPage));

            var result = await query.PaginateAsync<T>(page, perPage, _transaction, _timeout, cancellationToken);
            return result;
        }

        #endregion List Results

        #region Single Result

        public T Get<T>(object id) where T : class
        {
            var result = GetOrDefault<T>(id)
                ?? throw new EntityNotFoundException(typeof(T), id);

            return result;
        }

        public async Task<T> GetAsync<T>(object id, CancellationToken cancellationToken = default) where T : class
        {
            var result = (await GetOrDefaultAsync<T>(id, cancellationToken))
                ?? throw new EntityNotFoundException(typeof(T), id);

            return result;
        }

        public T GetOrDefault<T>(object id) where T : class
        {
            var idColumnName = QueryHelper.GetColumnName(QueryHelper.GetKeyProperty<T>(), _snakeCase);

            // var sql = Factory.Compiler.Compile(Query<T>().ToQuery().Where(idColumnName, id));

            var result = SelectAll<T>().ToQuery().Where(idColumnName, id).Get<T>(_transaction, _timeout);

            return ObservableProxy.Create(result.SingleOrDefault());
        }

        public async Task<T> GetOrDefaultAsync<T>(object id, CancellationToken cancellationToken = default) where T : class
        {
            var idColumnName = QueryHelper.GetColumnName(QueryHelper.GetKeyProperty<T>(), _snakeCase);

            // var sql = Factory.Compiler.Compile(Query<T>().ToQuery().Where(idColumnName, id));

            var result = await SelectAll<T>().ToQuery().Where(idColumnName, id).GetAsync<T>(_transaction, _timeout, cancellationToken);

            return ObservableProxy.Create(result.SingleOrDefault());
        }

        #endregion Single Result

        #region Save or Update

        /// <summary>
        /// Only virtual properties are saved.
        /// </summary>
        public object[] SaveOrUpdateBatch<T>(IEnumerable<T> objs) where T : class
        {
            var ids = new List<object>();

            foreach (var obj in objs)
                ids.Add(SaveOrUpdate<T>(obj));

            return ids.ToArray();
        }

        /// <summary>
        /// Only virtual properties are saved.
        /// </summary>
        public async Task<object[]> SaveOrUpdateBatchAsync<T>(IEnumerable<T> objs, CancellationToken cancellationToken = default) where T : class
        {
            var ids = new List<object>();

            foreach (var obj in objs)
                ids.Add(await SaveOrUpdateAsync<T>(obj, cancellationToken));

            return ids.ToArray();
        }

        /// <summary>
        /// Creates a new element in db (primary key is null) or updates an existing row (primary key has a value). Only virtual properties are saved.
        /// </summary>
        /// <exception cref="ArgumentNullException">obj required</exception>
        public object SaveOrUpdate<T>(T obj) where T : class
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            var idProperty = QueryHelper.GetKeyProperty<T>();

            var idColumnName = idProperty.Name;
            if (_snakeCase)
                idColumnName = idColumnName.ToSnakeCase();

            var id = idProperty.GetValue(obj);

            bool changed = true;

            bool proxy = ObservableProxy.IsProxy(obj);

            if (proxy)
            {
                changed = ObservableProxy.IsChanged(obj);
            }

            if (changed)
            {
                if (id == null)
                {
                    // Insert
                    QueryHelper.GenerateId(obj);

                    id = idProperty.GetValue(obj);

                    OnSaveOrUpdate(obj, true);
                    //var sql = Factory.Compiler.Compile(Query<T>().ToQuery().AsInsert(GetPropertyValues(obj)));
                    Query<T>().ToQuery()
                        .Insert(QueryHelper.GetPropertyValues(obj, snakeCase: _snakeCase), _transaction, _timeout);
                }
                else
                {
                    // Update
                    OnSaveOrUpdate(obj, false);
                    //var sql = Factory.Compiler.Compile(Query<T>().ToQuery().Where(idColumnName, id).AsUpdate(GetPropertyValues(obj, changedOnly: true)));
                    Query<T>().ToQuery()
                        .Where(idColumnName, id)
                        .Update(QueryHelper.GetPropertyValues(obj, changedOnly: true, snakeCase: _snakeCase), _transaction, _timeout);
                }
            }

            if (proxy && changed)
            {
                ObservableProxy.ClearChanged(obj);
            }

            return id;
        }

        /// <summary>
        /// Creates a new element in db (primary key is null) or updates an existing row (primary key has a value). Only virtual properties are saved.
        /// </summary>
        /// <exception cref="ArgumentNullException">obj required</exception>
        public async Task<object> SaveOrUpdateAsync<T>(T obj, CancellationToken cancellationToken = default) where T : class
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            var idProperty = QueryHelper.GetKeyProperty<T>();

            if (idProperty == null)
                throw new Exception($"The entity '{typeof(T).Name}' does not have [Key] property. Identifier is required.");

            var idColumnName = idProperty.GetCustomAttribute<QueryMan.Attributes.ColumnAttribute>()?.Name ?? idProperty.Name;
            
            if (_snakeCase)
                idColumnName = idColumnName.ToSnakeCase();

            var id = idProperty.GetValue(obj);

            var defaultId = idProperty.PropertyType.GetDefaultValue();

            var isAutoIncrement = idProperty.GetCustomAttribute<QueryMan.Attributes.KeyAttribute>().AutoIncrement;

            bool changed = id == null || id.Equals(defaultId);

            bool proxy = ObservableProxy.IsProxy(obj);

            if (!changed && proxy)
                changed = ObservableProxy.IsChanged(obj);

            if (changed)
            {
                if (id == null || id.Equals(defaultId)) // Insert
                {
                    // Insert
                    if (!isAutoIncrement)
                    {
                        QueryHelper.GenerateId(obj);

                        id = idProperty.GetValue(obj);
                    }

                    await OnSaveOrUpdateAsync(obj, true, cancellationToken);

                    // Debug
                    //var sql = Factory.Compiler.Compile(Query<T>().ToQuery().AsInsert(GetPropertyValues(obj)));

                    if (!isAutoIncrement)
                    {
                        await Query<T>().ToQuery()
                            .InsertAsync(QueryHelper.GetPropertyValues(obj, snakeCase: _snakeCase), _transaction, _timeout, cancellationToken);
                    }
                    else
                    {
                        // TODO: Replace with dynamicaly generated generic method based on type of id
                        if (idProperty.PropertyType != typeof(int))
                            throw new Exception("Only int is supported for auto incremental keys.");

                        id = await Query<T>().ToQuery()
                            .InsertGetIdAsync<int>(QueryHelper.GetPropertyValues(obj, exclude: new[] { idProperty.Name }, snakeCase: _snakeCase), _transaction, _timeout, cancellationToken);

                        idProperty.SetValue(obj, id);
                    }
                }
                else // Update
                {
                    // Update
                    await OnSaveOrUpdateAsync(obj, false, cancellationToken);

                    // Debug
                    //var sql = Factory.Compiler.Compile(Query<T>().ToQuery().Where(idColumnName, id).AsUpdate(GetPropertyValues(obj, changedOnly: true)));
                    
                    await Query<T>().ToQuery()
                        .Where(idColumnName, id)
                        .UpdateAsync(QueryHelper.GetPropertyValues(obj, changedOnly: true, snakeCase: _snakeCase), _transaction, _timeout, cancellationToken);
                }
            }

            if (proxy && changed)
            {
                ObservableProxy.ClearChanged(obj);
            }

            return id;
        }

        #endregion Save or Update

        #region Remove

        /// <summary>
        /// Remove entity from the database
        /// </summary>
        public int Remove<T>(object id)
        {
            var result = Query<T>()
                .ToQuery()
                .Where(QueryHelper.GetColumnName(QueryHelper.GetKeyProperty<T>(), _snakeCase), id)
                .Delete(_transaction, _timeout);

            return result;
        }

        /// <summary>
        /// Remove entity from the database
        /// </summary>
        public async Task<int> RemoveAsync<T>(object id, CancellationToken cancellationToken = default)
        {
            var result = await Query<T>()
                .ToQuery()
                .Where(QueryHelper.GetColumnName(QueryHelper.GetKeyProperty<T>(), _snakeCase), id)
                .DeleteAsync(_transaction, _timeout, cancellationToken);

            return result;
        }

        /// <summary>
        /// Remove entity from the database
        /// </summary>
        public int RemoveBatch<T>(IEnumerable<object> ids)
        {
            var result = Query<T>()
                .ToQuery()
                .WhereIn(QueryHelper.GetColumnName(QueryHelper.GetKeyProperty<T>(), _snakeCase), ids)
                .Delete(_transaction, _timeout);

            return result;
        }

        /// <summary>
        /// Remove entity from the database
        /// </summary>
        public async Task<int> RemoveBatchAsync<T>(IEnumerable<object> ids, CancellationToken cancellationToken = default)
        {
            var result = await Query<T>()
                .ToQuery()
                .WhereIn(QueryHelper.GetColumnName(QueryHelper.GetKeyProperty<T>(), _snakeCase), ids)
                .DeleteAsync(_transaction, _timeout, cancellationToken);

            return result;
        }

        #endregion Remove

        #region Audit Methods

        protected virtual async Task OnSaveOrUpdateAsync<T>(T obj, bool insert, CancellationToken cancellationToken = default)
        {
            // You can implement an audit here
        }

        protected virtual void OnSaveOrUpdate<T>(T obj, bool insert)
        {
            // You can implement an audit here
        }

        #endregion Audit Methods

        #region Private Methods

        #endregion Private Methods

        public void Dispose()
        {
            _transaction?.Dispose();
            _factory?.Dispose();
        }
    }
}
