﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace QueryMan
{
    public class TransactionActionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var service = context.HttpContext.RequestServices.GetRequiredService<QueryRunner>();

            service.BeginTransaction();
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            var service = context.HttpContext.RequestServices.GetRequiredService<QueryRunner>();

            if (!context.Canceled && context.Exception == null)
            {
                service.Commit();
            }
            else
            {
                service.Rollback();
            }
        }
    }

    public class TransactionActionFilterAttribute : Attribute, IActionFilter
    {
        private readonly static TransactionActionFilter _filter = new TransactionActionFilter();

        public void OnActionExecuting(ActionExecutingContext context)
        {
            _filter.OnActionExecuting(context);
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            _filter.OnActionExecuted(context);
        }
    }
}
