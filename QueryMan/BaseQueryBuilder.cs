﻿using CaseExtensions;
using QueryMan.Attributes;
using SqlKata;
using SqlKata.Compilers;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace QueryMan
{
    public abstract class BaseQueryBuilder<TBase, TOutput> where TBase : BaseQuery<TBase> where TOutput : BaseQueryBuilder<TBase, TOutput>
    {
        protected BaseQuery<TBase> _baseQuery;
        protected readonly Compiler _compiler;
        protected readonly bool _snakeCase;

        protected BaseQueryBuilder(TBase query, Compiler compiler, bool snakeCase = false)
        {
            _baseQuery = query;
            _snakeCase = snakeCase;
        }

        #region Clone

        public abstract TOutput CreateNew();

        public abstract TOutput CreateClone();

        #endregion Clone

        #region From

        internal TOutput From<T>()
        {
            _baseQuery.From($"{GetTableName<T>()}");
            return (TOutput)this;
        }

        public TOutput From<T>(Expression<Func<T>> alias)
        {
            _baseQuery.From($"{GetTableName<T>()} AS {GetAliasName(alias)}");
            return (TOutput)this;
        }

        #endregion From

        #region Where

        public TOutput WhereRaw(string query)
        {
            _baseQuery.WhereRaw(query);
            return (TOutput)this;
        }

        public TOutput OrWhereRaw(string query)
        {
            _baseQuery.OrWhereRaw(query);
            return (TOutput)this;
        }

        public TOutput Where<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.Where($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput Where<T>(Expression<Func<T>> column, QueryBuilder query, string op = "=")
        {
            _baseQuery.Where($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, query.ToQuery());
            return (TOutput)this;
        }

        public TOutput OrWhere<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhere($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhere<T>(Expression<Func<T>> column, QueryBuilder query, string op = "=")
        {
            _baseQuery.OrWhere($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, query.ToQuery());
            return (TOutput)this;
        }

        public TOutput WhereNot<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.WhereNot($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereNot<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhereNot($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput WhereColumns<T1, T2>(Expression<Func<T1>> column1, Expression<Func<T2>> column2, string op = "=")
        {
            _baseQuery.WhereColumns(
                $"{GetAliasNameFromPropery(column1)}.{GetPropertyName(column1)}",
                op,
                $"{GetAliasNameFromPropery(column2)}.{GetPropertyName(column2)}");
            return (TOutput)this;
        }

        public TOutput OrWhereColumns<T1, T2>(Expression<Func<T1>> column1, Expression<Func<T2>> column2, string op = "=")
        {
            _baseQuery.OrWhereColumns(
                $"{GetAliasNameFromPropery(column1)}.{GetPropertyName(column1)}",
                op,
                $"{GetAliasNameFromPropery(column2)}.{GetPropertyName(column2)}");
            return (TOutput)this;
        }

        public TOutput WhereNull<T>(Expression<Func<T>> column)
        {
            _baseQuery.WhereNull($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput OrWhereNull<T>(Expression<Func<T>> column)
        {
            _baseQuery.OrWhereNull($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput WhereNotNull<T>(Expression<Func<T>> column)
        {
            _baseQuery.WhereNotNull($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput OrWhereNotNull<T>(Expression<Func<T>> column)
        {
            _baseQuery.OrWhereNotNull($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput WhereTrue<T>(Expression<Func<T>> column)
        {
            _baseQuery.WhereTrue($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput OrWhereTrue<T>(Expression<Func<T>> column)
        {
            _baseQuery.OrWhereTrue($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput WhereFalse<T>(Expression<Func<T>> column)
        {
            _baseQuery.WhereFalse($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput OrWhereFalse<T>(Expression<Func<T>> column)
        {
            _baseQuery.OrWhereFalse($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}");
            return (TOutput)this;
        }

        public TOutput WhereLike<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereLike($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereLike<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereLike($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereNotLike<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereNotLike($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereNotLike<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereNotLike($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereStarts<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereStarts($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereStarts<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereStarts($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereNotStarts<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereNotStarts($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereNotStarts<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereNotStarts($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereEnds<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereEnds($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereEnds<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereEnds($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereNotEnds<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereNotEnds($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereNotEnds<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereNotEnds($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereContains<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereContains($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereContains<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereContains($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereNotContains<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.WhereNotContains($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput OrWhereNotContains<T>(Expression<Func<T>> column, object value, bool caseSensitive = false)
        {
            _baseQuery.OrWhereNotContains($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", value, caseSensitive: caseSensitive);
            return (TOutput)this;
        }

        public TOutput WhereBetween<T, TValue>(Expression<Func<T>> column, TValue lower, TValue higher)
        {
            _baseQuery.WhereBetween($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", lower, higher);
            return (TOutput)this;
        }

        public TOutput OrWhereBetween<T, TValue>(Expression<Func<T>> column, TValue lower, TValue higher)
        {
            _baseQuery.OrWhereBetween($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", lower, higher);
            return (TOutput)this;
        }

        public TOutput WhereNotBetween<T, TValue>(Expression<Func<T>> column, TValue lower, TValue higher)
        {
            _baseQuery.WhereNotBetween($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", lower, higher);
            return (TOutput)this;
        }

        public TOutput OrWhereNotBetween<T, TValue>(Expression<Func<T>> column, TValue lower, TValue higher)
        {
            _baseQuery.OrWhereNotBetween($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", lower, higher);
            return (TOutput)this;
        }

        public TOutput WhereIn<T, TValue>(Expression<Func<T>> column, IEnumerable<TValue> values)
        {
            _baseQuery.WhereIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", values);
            return (TOutput)this;
        }

        public TOutput OrWhereIn<T, TValue>(Expression<Func<T>> column, IEnumerable<TValue> values)
        {
            _baseQuery.OrWhereIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", values);
            return (TOutput)this;
        }

        public TOutput WhereNotIn<T, TValue>(Expression<Func<T>> column, IEnumerable<TValue> values)
        {
            _baseQuery.WhereNotIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", values);
            return (TOutput)this;
        }

        public TOutput OrWhereNotIn<T, TValue>(Expression<Func<T>> column, IEnumerable<TValue> values)
        {
            _baseQuery.OrWhereNotIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", values);
            return (TOutput)this;
        }

        public TOutput WhereIn<T, TValue>(Expression<Func<T>> column, QueryBuilder query)
        {
            _baseQuery.WhereIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", query.ToQuery());
            return (TOutput)this;
        }

        public TOutput OrWhereIn<T, TValue>(Expression<Func<T>> column, QueryBuilder query)
        {
            _baseQuery.OrWhereIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", query.ToQuery());
            return (TOutput)this;
        }

        public TOutput WhereNotIn<T, TValue>(Expression<Func<T>> column, QueryBuilder query)
        {
            _baseQuery.WhereNotIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", query.ToQuery());
            return (TOutput)this;
        }

        public TOutput OrWhereNotIn<T, TValue>(Expression<Func<T>> column, QueryBuilder query)
        {
            _baseQuery.OrWhereNotIn($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", query.ToQuery());
            return (TOutput)this;
        }

        public TOutput WhereSub(QueryBuilder query, object value, string op = "=")
        {
            _baseQuery.WhereSub(query.ToQuery(), op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereSub(QueryBuilder query, object value, string op = "=")
        {
            _baseQuery.OrWhereSub(query.ToQuery(), op, value);
            return (TOutput)this;
        }

        public TOutput WhereExists(QueryBuilder query)
        {
            _baseQuery.WhereExists(query.ToQuery());
            return (TOutput)this;
        }

        public TOutput OrWhereExists(QueryBuilder query)
        {
            _baseQuery.OrWhereExists(query.ToQuery());
            return (TOutput)this;
        }

        public TOutput WhereNotExists(QueryBuilder query)
        {
            _baseQuery.WhereNotExists(query.ToQuery());
            return (TOutput)this;
        }

        public TOutput OrWhereNotExists(QueryBuilder query)
        {
            _baseQuery.OrWhereNotExists(query.ToQuery());
            return (TOutput)this;
        }

        public TOutput WhereDatePart<T>(string part, Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.WhereDatePart(part, $"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereDatePart<T>(string part, Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhereDatePart(part, $"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput WhereNotDatePart<T>(string part, Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.WhereNotDatePart(part, $"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereNotDatePart<T>(string part, Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhereNotDatePart(part, $"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput WhereDate<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.WhereDate($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereDate<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhereDate($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput WhereNotDate<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.WhereNotDate($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereNotDate<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhereNotDate($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput WherTime<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.WhereTime($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereTime<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhereTime($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput WhereNotTime<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.WhereNotTime($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        public TOutput OrWhereNotTime<T>(Expression<Func<T>> column, object value, string op = "=")
        {
            _baseQuery.OrWhereNotTime($"{GetAliasNameFromPropery(column)}.{GetPropertyName(column)}", op, value);
            return (TOutput)this;
        }

        #endregion Where

        #region Public Methods

        public TBase ToQuery()
        {
            return (TBase)_baseQuery;
        }

        public string GetColumnName<T>(Expression<Func<T>> property)
        {
            return GetPropertyName<T>(property);
        }

        #endregion Public Methods

        #region Private Methods

        protected string GetTableName<T>()
        {
            var tableName = typeof(T).GetCustomAttribute<TableAttribute>()?.Name;

            if (string.IsNullOrWhiteSpace(tableName))
                tableName = _snakeCase ? typeof(T).Name.ToSnakeCase() : typeof(T).Name;

            return tableName;
        }

        protected string GetAliasName<T>(Expression<Func<T>> alias)
        {
            return GetPropertyName(alias, snakeCase: false);
        }

        protected string GetAliasNameFromPropery<T>(Expression<Func<T>> property)
        {
            return GetPropertyName(property, parent: true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        /// <param name="snake"></param>
        /// <param name="parent">Get parent member name (eg. customer.Id will return "customer" instead of "ID")</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        protected string GetPropertyName<T>(Expression<Func<T>> property, bool parent = false, bool? snakeCase = null)
        {
            var propertyName = GetMemberName(property, parent: parent);

            if (propertyName != null)
                return snakeCase ?? _snakeCase ? propertyName.ToSnakeCase() : propertyName;
            else
                throw new ArgumentException($"The expression cannot be evaluated");
        }

        private string GetMemberName(Expression expression, bool parent = false)
        {
            switch (expression.NodeType)
            {
                case ExpressionType.MemberAccess:
                    return parent
                        ? ((MemberExpression)((MemberExpression)expression).Expression).Member.Name
                        : ((MemberExpression)expression).Member.Name;
                case ExpressionType.Convert:
                    return GetMemberName(((UnaryExpression)expression).Operand, parent: parent);
                case ExpressionType.Lambda:
                    return (parent
                            ? ((((LambdaExpression)expression).Body as MemberExpression)?.Expression as MemberExpression)?.Member.Name
                            : (((LambdaExpression)expression).Body as MemberExpression)?.Member.Name
                        ) ?? throw new NotSupportedException(expression.NodeType.ToString(), new Exception($"Cannot get member name from expression {expression}.")); ;
                default:
                    throw new NotSupportedException(expression.NodeType.ToString(),
                        new Exception($"Cannot get member name from expression {expression}."));
            }
        }

        #endregion Private Methods
    }
}
