﻿using CaseExtensions;
using Microsoft.Extensions.DependencyInjection;
using QueryMan.Observable;
using SqlKata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace QueryMan
{
    public static class QueryHelper
    {
        public static IServiceCollection ConfigureQueryMan(this IServiceCollection services, QueryManConfig config)
        {
            services.AddSingleton(config);
            services.AddScoped((s) =>
            {
                var cfg = s.GetRequiredService<QueryManConfig>();
                return new QueryRunner(
                    cfg.Connection?.Invoke(s) ?? throw new ArgumentNullException(nameof(config.Connection), $"The Connection is required to activate QueryMan.{nameof(QueryRunner)}. See {nameof(QueryManConfig)}."),
                    cfg.Compiler?.Invoke(s) ?? throw new ArgumentNullException(nameof(config.Compiler), $"The Compiler is required to activate QueryMan.{nameof(QueryRunner)}. See {nameof(QueryManConfig)}."),
                    cfg.SnakeCase,
                    cfg.Timeout,
                    cfg.IsolationLevel
                );
            });

            return services;
        }

        public static PropertyInfo GetKeyProperty<T>()
        {
            var foundIdProperties = typeof(T).GetProperties()
                .Where(x => x.GetCustomAttribute<QueryMan.Attributes.KeyAttribute>() != null)
                .ToList();

            if (foundIdProperties.Count == 0)
                throw new ArgumentException($"The provided entity '{typeof(T)}' does not have a primary key column.");

            if (foundIdProperties.Count > 1)
                throw new ArgumentException($"The provided entity '{typeof(T)}' has more than one primary key column.");

            return foundIdProperties.First();
        }

        public static string GetColumnName(PropertyInfo property, bool snakeCase)
        {
            return property.GetCustomAttribute<QueryMan.Attributes.ColumnAttribute>()?.Name
                ?? (snakeCase ? property.Name.ToSnakeCase() : property.Name);
        }

        public static IDictionary<string, string> GetColumns<T>(bool snakeCase)
        {
            return GetProperties<T>().ToDictionary(x => x.Name, x => snakeCase ? x.Name.ToSnakeCase() : x.Name);
        }

        public static PropertyInfo[] GetProperties<T>()
        {
            return typeof(T).GetProperties();
        }

        public static IDictionary<string, object> GetPropertyValues<T>(T obj, bool changedOnly = false, string[] exclude = null, bool snakeCase = true) where T : class
        {
            var columns = new Dictionary<string, object>();

            IEnumerable<PropertyInfo> properties;

            if (changedOnly && ObservableProxy.IsProxy(obj))
            {
                var changedProps = ObservableProxy.ChangedProperties(obj);
                properties = GetProperties<T>().Where(x => changedProps.Contains(x.Name));
            }
            else
            {
                properties = GetProperties<T>();
            }

            foreach (var prop in properties)
            {
                if (prop.GetCustomAttribute<IgnoreAttribute>() != null)
                    continue;

                if (exclude != null && exclude.Contains(prop.Name))
                    continue;

                var columnName = prop.GetCustomAttribute<QueryMan.Attributes.ColumnAttribute>()?.Name 
                    ?? (snakeCase ? prop.Name.ToSnakeCase() : prop.Name);

                columns.Add(columnName, prop.GetValue(obj));
            }

            return columns;
        }

        public static void GenerateId<T>(T obj)
        {
            var keyProperty = GetKeyProperty<T>();
            var keyAttribute = keyProperty.GetCustomAttribute<QueryMan.Attributes.KeyAttribute>();
            if (keyAttribute.Generator != null)
                keyProperty.SetValue(obj, keyAttribute.Generator.Generate());
        }

        public static bool IsEnumerable<T>()
        {
            return IsEnumerable(typeof(T));
        }

        public static bool IsEnumerable(this Type type)
        {
            return typeof(System.Collections.IEnumerable).IsAssignableFrom(type);
        }

        public static bool IsTypeOf<T, OfType>()
        {
            return IsTypeOf(typeof(T), typeof(OfType));
        }

        public static bool IsTypeOf<T>(Type ofType)
        {
            return IsTypeOf(typeof(T), ofType);
        }

        public static bool IsTypeOf(this Type type, Type ofType)
        {
            return ofType.IsAssignableFrom(type);
        }

    }
}
