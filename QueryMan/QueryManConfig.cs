﻿using SqlKata.Compilers;
using System;
using System.Data;

namespace QueryMan
{
    public class QueryManConfig
    {
        public QueryManConfig()
        {
            
        }

        /// <summary>
        /// Desired database connection (eg. System.Data.SQLite.SQLiteConnection)
        /// </summary>
        public Func<IServiceProvider, IDbConnection> Connection { get; set; }

        /// <summary>
        /// Desired compiler (eg. SqlKata.Compilers.SqliteCompiler)
        /// </summary>
        public Func<IServiceProvider, Compiler> Compiler { get; set; }

        /// <summary>
        /// Columns are represented in snake case.
        /// </summary>
        public bool SnakeCase { get; set; }

        /// <summary>
        /// Sql query execution timeout in seconds (default 30)
        /// </summary>
        public int Timeout { get; set; } = 30;

        /// <summary>
        /// Default transactional isolation level (default ReadCommitted)
        /// </summary>
        public IsolationLevel IsolationLevel { get; set; } = IsolationLevel.ReadCommitted;
    }
}
