﻿using QueryMan.Abstracts;
using QueryMan.Helpers;

namespace QueryMan.IdentityGenerator
{
    public class KeyVarChar16Generator : IKeyGenerator
    {
        public object Generate()
        {
            return RandomStringGenerator.WebHash();
        }
    }
}
