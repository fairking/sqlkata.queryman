﻿using QueryMan.Abstracts;
using System;

namespace QueryMan.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class KeyAttribute : Attribute
    {
        private readonly Type _generator;
        private IKeyGenerator _generatorInstance;

        /// <summary>
        /// Creates an auto incremental identifier (only int is supported at the moment)
        /// </summary>
        public KeyAttribute()
        {
            AutoIncrement = true;
        }

        /// <summary>
        /// Creates a generated identifier (generator is required). Existing generators can be used at QueryMan.IdentityGenerator.
        /// </summary>
        public KeyAttribute(Type generator)
        {
            if (generator == null)
                throw new ArgumentNullException(nameof(generator));

            if (!typeof(IKeyGenerator).IsAssignableFrom(generator))
                throw new ArgumentException($"The argument must derive from {nameof(IKeyGenerator)}", nameof(generator));

            _generator = generator;
        }

        public bool AutoIncrement { get; protected set; }

        public IKeyGenerator Generator
        {
            get
            {
                if (_generatorInstance == null && _generator != null)
                    _generatorInstance = (IKeyGenerator)Activator.CreateInstance(_generator);
                return _generatorInstance;
            }
        }
    }
}
