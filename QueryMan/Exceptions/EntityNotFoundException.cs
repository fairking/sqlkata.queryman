﻿using System;

namespace QueryMan.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(Type entityType, object id) : base($"Entity '{entityType.Name}' with Id '{id}' not found.")
        {

        }
    }
}
