﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Castle.DynamicProxy;

namespace QueryMan.Observable
{
    internal class ObservableInterceptor : IInterceptor
    {
        public bool IsChanged { get; protected set; }

        private HashSet<string> _changedProperties = new HashSet<string>();

        public IEnumerable<string> ChangedProperties => _changedProperties;

        public void Intercept(IInvocation invocation)
        {
            // only virtual properties are landed here
            if (IsSetter(invocation.Method))
            {
                var propertyName = invocation.Method.Name.Substring(4);
                var property = invocation.TargetType.GetProperty(propertyName);
                var oldValue = property.GetValue(invocation.InvocationTarget);
                var newValue = invocation.Arguments[0];
                if (!object.Equals(oldValue, newValue))
                {
                    IsChanged = true;
                    _changedProperties.Add(propertyName);
                }
            }
            invocation.Proceed();
        }

        public void ClearChanges()
        {
            IsChanged = false;
            _changedProperties.Clear();
        }

        private bool IsSetter(MethodInfo method)
        {
            return method.IsSpecialName && method.Name.StartsWith("set_", StringComparison.OrdinalIgnoreCase);
        }
    }
}
