# QueryMan

**Simple and very concrete**, fluent query manager based on [SQLKata](https://github.com/sqlkata/querybuilder) and [Dapper](https://github.com/DapperLib/Dapper).

# Features

- Fluent queries (you have more freedom than with `Linq`, `HQL` or `QueryOver`)
- Save/Update/Delete entities
- Transactions
- Pagination

# How to use

Init QueryMan (freeman way):
``` c#
using QueryMan;
using SqlKata.Compilers;
using System.Data.SQLite;

// Method 1
using (var Db = new QueryRunner(new SQLiteConnection(ConnectionString), new SqliteCompiler()))
{
    // Man, do you queries here
}

// Method 2
Db = new QueryRunner(new SQLiteConnection(ConnectionString), new SqliteCompiler());

// Man, do you queries here

Db.Dispose();
```
> Please remember to always `Dispose` your QueryRunner instance when you finish.

Init QueryMan (asp.net core):
``` c#
using QueryMan;
using SqlKata.Compilers;
using System.Data.SQLite;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.ConfigureQueryMan(new QueryManConfig()
        {
            Connection = (s) => new SQLiteConnection(s.GetRequiredService<IConfiguration>().GetConnectionString("DefaultConnection")),
            Compiler = (s) => new SqliteCompiler(),
        });

        // Optionally you can globally enable database transactions against every http request.
        // As alternative use [TransactionActionFilter] attribute against your controllers or actions.
        // It will create a new database transaction before every request 
        // and perform a commit when the action (request) executed without erros, otherwise it will rollback all db changes.
        services.AddControllers(o => o.Filters.Add<TransactionActionFilter>());
    }
}

// Your controller
public class CustomerController : ControllerBase
{
    private readonly QueryRunner _db;

    public PageBController(QueryRunner db)
    {
        _db = db;
    }

    [HttpGet]
    public Customer Get(string id)
    {
        return _db.Get<Customer>(id);
    }

    [HttpPost]
    [TransactionActionFilter] // It will execute sql queries inside the transaction.
    public void Put(Customer customer)
    {
        _db.SaveOrUpdate(customer);
    }
}
```

The preffered way to have an entity against your datatable:
``` c#
[Table("customers")]
public class Customer
{
    protected Customer() // Used by ObservableProxy to create a proxy object and filling with data from database.
    {

    }

    public Customer(string name) // Used by developers to create a new object before it will be saved to the db. See how the `mandatory Name` is implemented by `protected set`.
    {
        SetName(name);
    }

    public virtual void SetName(string name)
    {
        if (string.IsNullOrEmpty(name))
            throw new ArgumentNullException(nameof(name));
        
        Name = name;
    }

    [Key] // Primary key column
    public virtual string Id { get; protected set; } // "virtual" allows proxy to override properties, otherwise this property won't be treated as a database column and won't pull or persist any data when you pull data as proxies. 

    [Column("CustomerName")] // Custom column in the database (by defaut it will use a property name).
    public virtual string Name { get; protected set; }
}
```

Save to db:
``` c#
var customer = new Customer("Customer 1");

Db.BeginTransaction();

await Db.SaveOrUpdateAsync(customer);

Db.Commit();

customer = await Db.GetAsync<Customer>(customer.Id);

Assert.Equal("Customer 1", customer.Name);
```

Get by Id:
``` c#
var customer = await Db.GetAsync<Customer>(id);
```

Get from db:
``` c#
Customer customer = null;
(string CustomerId, string CustomerName) queryResult = default;

var query = Db.Query<Customer>()
    .Select(() => customer.Id, () => queryResult.CustomerId)
    .Select(() => customer.Name, () => queryResult.CustomerName)
    ;

var result = await Db.ToListNoProxy<(string CustomerId, string CustomerName)>(query);

AssertHelper.CollectionContainsAll(result,
    item => item.CustomerName == "Customer 1"
);
```

Get from db with GroupBy:
``` c#
Contact contact = null;
Customer customer = null;
(string CustomerName, int ContactCount) queryResult = default;

var query = Db.Query(() => customer)
    .LeftJoin(() => contact, () => contact.CustomerId, () => customer.Id)
    .Select(() => customer.Name, () => queryResult.CustomerName)
    .SelectCount(() => contact.Id, () => queryResult.ContactCount)
    .GroupBy()
    ;

var result = await Db.ToListNoProxy<(string CustomerName, int ContactCount)>(query);

AssertHelper.CollectionContainsAll(result,
    item => item.CustomerName == "Customer 1a" && item.ContactCount == 1,
    item => item.CustomerName == "Customer 2" && item.ContactCount == 0
);
```

Batch Update:
``` c#
Customer customer = null;

var query = Db.SelectAll(() => customer);

var result = await Db.ToList<Customer>(query);

foreach (var (c, i) in result.Select((c, i) => (c, i)))
{
    c.SetName($"Customer No {i+1}");
}

Db.BeginTransaction();

await Db.SaveOrUpdateAsync(result);

Db.Commit();

```

# Nuget

[SqlKata.QueryMan](https://www.nuget.org/packages/SqlKata.QueryMan/)

# How to contribute

If you have any issues please provide us with Unit Test Example.

Please create an issue ticket to become a contributer.

# Donations

Donate with [nano](https://nano.org).

[![SqlKata.QueryMan Donations](https://gitlab.com/fairking/sqlkata.queryman/-/raw/master/Resources/Donations_QRCode_nano_1sygjbke.png)](https://nanocrawler.cc/explorer/account/nano_1sygjbkepdcu5diiekf15ar6m6utfgf9rr9tkd6zi8mkq7yza34kiyjpgt9g)

Thank you!
