﻿using FluentMigrator;
using System;

namespace QueryMan.Tests.Migrations.SnakeCase
{
    [Tags("SnakeCase")]
    [Migration(1)]
    public class M001_BasicDbInit : Migration
    {
        public override void Up()
        {
            Create.Table("customers")
                .WithColumn("id").AsAnsiString(16).NotNullable().PrimaryKey()
                .WithColumn("name").AsString(50).NotNullable()
                ;

            Create.Table("contacts")
                .WithColumn("id").AsAnsiString(16).NotNullable().PrimaryKey()
                .WithColumn("first_name").AsString(50).NotNullable()
                .WithColumn("last_name").AsString(50).NotNullable()
                .WithColumn("customer_id").AsAnsiString(16).Nullable().ForeignKey("fk_contacts_customer_id", "customers", "id")
                ;
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
