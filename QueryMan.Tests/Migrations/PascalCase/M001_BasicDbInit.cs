﻿using FluentMigrator;
using System;

namespace QueryMan.Tests.Migrations.PascalCase
{
    [Tags("PascalCase")]
    [Migration(1)]
    public class M001_BasicDbInit : Migration
    {
        public override void Up()
        {
            Execute.Sql("CREATE TABLE Customers(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT NOT NULL);");

            //Create.Table("Customers")
            //    .WithColumn("Id").AsInt32().Identity().PrimaryKey()
            //    .WithColumn("Name").AsString(50).NotNullable()
            //    ;

            Execute.Sql(@"CREATE TABLE Contacts(
                Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                FirstName TEXT NOT NULL, 
                LastName TEXT NOT NULL, 
                CustomerId INTEGER NULL,
                FOREIGN KEY (CustomerId) REFERENCES Customers(Id)
            );");

            //Create.Table("Contacts")
            //    .WithColumn("Id").AsInt32().Identity().PrimaryKey()
            //    .WithColumn("FirstName").AsString(50).NotNullable()
            //    .WithColumn("LastName").AsString(50).NotNullable()
            //    .WithColumn("CustomerId").AsAnsiString(16).Nullable().ForeignKey("FK_Contacts_CustomerId", "Customers", "Id")
            //    ;
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }
    }
}
