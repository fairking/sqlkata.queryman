﻿using QueryMan.Tests.Fixtures;
using Xunit;

namespace QueryMan.Tests.Base
{
    public abstract class SnakeCaseDbTest : IClassFixture<SnakeCaseDbFixture>
    {
        private readonly SnakeCaseDbFixture _fixture;
        protected QueryRunner Db => _fixture.Db;

        public SnakeCaseDbTest(SnakeCaseDbFixture fixture)
        {
            this._fixture = fixture;
        }
    }
}
