﻿using SqlKata.Compilers;

namespace QueryMan.Tests.Base
{
    public abstract class BaseTest
    {
        protected QueryBuilder GetQueryBuilder(bool snakeCase)
        {
            return new QueryBuilder(new SqlServerCompiler(), snakeCase);
        }
    }
}
