﻿using QueryMan.Tests.Fixtures;
using Xunit;

namespace QueryMan.Tests.Base
{
    public abstract class PascalCaseDbTest : IClassFixture<PascalCaseDbFixture>
    {
        private readonly PascalCaseDbFixture _fixture;
        protected QueryRunner Db => _fixture.Db;

        public PascalCaseDbTest(PascalCaseDbFixture fixture)
        {
            this._fixture = fixture;
        }
    }
}
