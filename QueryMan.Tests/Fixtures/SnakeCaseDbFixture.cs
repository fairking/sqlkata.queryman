﻿using Microsoft.Extensions.DependencyInjection;
using QueryMan.Helpers;
using SqlKata.Compilers;
using System;
using FluentMigrator.Runner;
using System.Data.SQLite;
using QueryMan.Tests.Migrations.SnakeCase;
using FluentMigrator.Runner.Initialization;

namespace QueryMan.Tests.Fixtures
{
    public class SnakeCaseDbFixture : BaseDbFixture
    {
        public SnakeCaseDbFixture() : base("SnakeCase", true)
        {

        }
    }
}
