﻿using Microsoft.Extensions.DependencyInjection;
using QueryMan.Helpers;
using SqlKata.Compilers;
using System;
using FluentMigrator.Runner;
using System.Data.SQLite;
using QueryMan.Tests.Migrations.SnakeCase;
using FluentMigrator.Runner.Initialization;
using System.Diagnostics;

namespace QueryMan.Tests.Fixtures
{
    public abstract class BaseDbFixture
    {
        private readonly string ConnectionString;
        public readonly QueryRunner Db;
        public readonly IServiceProvider _services;

        public BaseDbFixture(string tag, bool snakeCase)
        {
            // QueryMan Setup
            var dbId = RandomStringGenerator.WebHash();
            //if (Debugger.IsAttached)
                ConnectionString = $"Data Source=c:\\temp\\testdb_{dbId}.db;Version=3;New=True;DateTimeKind=Utc";
            //else
                //ConnectionString = $"FullUri=file:memorydb_{dbId}.db?mode=memory&cache=shared;DateTimeKind=Utc";

            Db = new QueryRunner(
                new SQLiteConnection(ConnectionString),
                new SqliteCompiler(),
                snakeCase: snakeCase
            );

            // Migrations Setup
            _services = new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    // Add SQLite support to FluentMigrator
                    .AddSQLite()
                    // Set the connection string
                    .WithGlobalConnectionString(ConnectionString)
                    // Define the assembly containing the migrations
                    .ScanIn(typeof(M001_BasicDbInit).Assembly).For.Migrations())
                // Enable logging to console in the FluentMigrator way
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .Configure<RunnerOptions>(opt => {
                    opt.Tags = new[] { tag };
                })
                // Build the service provider
                .BuildServiceProvider(false);

            // Migrations Run
            using (var scope = _services.CreateScope())
            {
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                var migrations = runner.MigrationLoader.LoadMigrations();
                runner.MigrateUp();
            }
        }
    }
}
