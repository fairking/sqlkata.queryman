﻿using QueryMan.Attributes;
using QueryMan.IdentityGenerator;

namespace QueryMan.Tests.Entities.SnakeCase
{
    public class BaseEntity
    {
        protected BaseEntity()
        {

        }

        [Key(typeof(KeyVarChar16Generator))]
        public virtual string Id { get; protected set; }
    }
}
