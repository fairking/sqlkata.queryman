﻿using QueryMan.Attributes;
using System;

namespace QueryMan.Tests.Entities.PascalCase
{
    public class Customers : BaseEntity
    {
        protected Customers() : base()
        {

        }

        public Customers(string name) : this()
        {
            SetName(name);
        }

        #region Properties

        public virtual string Name { get; protected set; }

        #endregion Properties

        #region Public Methods

        public virtual void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            Name = name;
        }

        #endregion Public Methods
    }
}
