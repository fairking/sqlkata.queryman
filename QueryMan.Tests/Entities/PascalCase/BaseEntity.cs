﻿using QueryMan.Attributes;
using QueryMan.IdentityGenerator;

namespace QueryMan.Tests.Entities.PascalCase
{
    public class BaseEntity
    {
        protected BaseEntity()
        {

        }

        [Key]
        public virtual int Id { get; protected set; }
    }
}
