﻿using QueryMan.Tests.Base;
using QueryMan.Tests.Entities.PascalCase;
using QueryMan.Tests.Fixtures;
using QueryMan.Tests.Helpers;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace QueryMan.Tests.AllTests.QueryRunnerTests
{
    [TestCaseOrderer("QueryMan.Tests.Helpers.AlphabeticalTestOrderer", "QueryMan.Tests")]
    public class PascalCaseBasicTests : PascalCaseDbTest, IClassFixture<BagFixture>
    {
        private readonly BagFixture _bag;

        public PascalCaseBasicTests(PascalCaseDbFixture db, BagFixture bag) : base(db)
        {
            _bag = bag;
        }

        [Fact]
        public async Task T01_CreateCustomer()
        {
            var customer = new Customers("Customer 1");
            var customer2 = new Customers("Customer 2");

            Db.BeginTransaction();

            await Db.SaveOrUpdateAsync(customer);
            await Db.SaveOrUpdateAsync(customer2);

            _bag.SetValue("Customer1Id", customer.Id);

            Db.Commit();

            customer = await Db.GetAsync<Customers>(customer.Id);

            Assert.Equal("Customer 1", customer.Name);
        }

        [Fact]
        public async Task T02_UpdateCustomer()
        {
            var customerId = _bag.GetValue<int>("Customer1Id");

            Db.BeginTransaction();

            var customer = await Db.GetAsync<Customers>(customerId);

            customer.SetName("Customer 1a");

            await Db.SaveOrUpdateAsync(customer);

            Db.Commit();

            customer = await Db.GetAsync<Customers>(customer.Id);

            Assert.Equal("Customer 1a", customer.Name);
        }

        [Fact]
        public async Task T03_GetCustomerList()
        {
            Customers customer = null;
            (string CustomerId, string CustomerName) queryResult = default;

            var query = Db.Query(() => customer)
                .Select(() => customer.Id, () => queryResult.CustomerId)
                .Select(() => customer.Name, () => queryResult.CustomerName)
                ;

            var result = await Db.ToListNoProxyAsync<(string CustomerId, string CustomerName)>(query);

            AssertHelper.CollectionContainsAll(result,
                item => item.CustomerName == "Customer 1a",
                item => item.CustomerName == "Customer 2"
            );
        }

        [Fact]
        public async Task T04_CreateContact()
        {
            var contact = new Contact("Name 1", "Surname 1");
            var contact2 = new Contact("Name 2", "Surname 2")
            {
                CustomerId = _bag.GetValue<int>("Customer1Id"),
            };

            Db.BeginTransaction();

            await Db.SaveOrUpdateAsync(contact);
            await Db.SaveOrUpdateAsync(contact2);

            Db.Commit();

            contact = await Db.GetAsync<Contact>(contact.Id);
            contact2 = await Db.GetAsync<Contact>(contact2.Id);

            Assert.Equal("Name 1", contact.FirstName);
            Assert.Equal("Surname 1", contact.LastName);
            Assert.Equal(0, contact.CustomerId);

            Assert.Equal("Name 2", contact2.FirstName);
            Assert.Equal("Surname 2", contact2.LastName);
            Assert.NotEqual(0, contact2.CustomerId);
        }

        [Fact]
        public async Task T05_GetContactList()
        {
            Contact contact = null;
            Customers customer = null;
            (string FirstName, string LastName, string CustomerName) queryResult = default;

            var query = Db.Query(() => contact)
                .LeftJoin(() => customer, () => customer.Id, () => contact.CustomerId)
                .Select(() => contact.FirstName, () => queryResult.FirstName)
                .Select(() => contact.LastName, () => queryResult.LastName)
                .Select(() => customer.Name, () => queryResult.CustomerName)
                ;

            var result = await Db.ToListNoProxyAsync<(string FirstName, string LastName, string CustomerName)>(query);

            AssertHelper.CollectionContainsAll(result,
                item => item.FirstName == "Name 1" && item.LastName == "Surname 1" && item.CustomerName == null,
                item => item.FirstName == "Name 2" && item.LastName == "Surname 2" && item.CustomerName == "Customer 1a"
            );
        }

        [Fact]
        public async Task T05_GroupCustomers()
        {
            Contact contact = null;
            Customers customer = null;
            (string CustomerName, int ContactCount) queryResult = default;

            var query = Db.Query(() => customer)
                .LeftJoin(() => contact, () => contact.CustomerId, () => customer.Id)
                .Select(() => customer.Name, () => queryResult.CustomerName)
                .SelectCount(() => contact.Id, () => queryResult.ContactCount)
                .GroupBy()
                ;

            var result = await Db.ToListNoProxyAsync<(string CustomerName, int ContactCount)>(query);

            AssertHelper.CollectionContainsAll(result,
                item => item.CustomerName == "Customer 1a" && item.ContactCount == 1,
                item => item.CustomerName == "Customer 2" && item.ContactCount == 0
            );
        }

        [Fact]
        public async Task T06_OrderCustomers()
        {
            // ASC
            {
                Customers customer = null;

                var query = Db.SelectAll(() => customer)
                    .OrderBy(() => customer.Name)
                    ;

                var result = await Db.ToListAsync<Customers>(query);

                Assert.Equal(2, result.Count());
                Assert.Equal("Customer 1a", result.First().Name);
            }

            // DESC
            {
                Customers customer = null;

                var query = Db.SelectAll(() => customer)
                    .OrderByDesc(() => customer.Name)
                    ;

                var result = await Db.ToListAsync<Customers>(query);

                Assert.Equal(2, result.Count());
                Assert.Equal("Customer 2", result.First().Name);
            }
        }

        [Fact]
        public async Task T07_BatchUpdate()
        {
            Customers customer = null;

            var query = Db.SelectAll(() => customer)
                .OrderBy(() => customer.Name)
                ;

            var result = await Db.ToListAsync<Customers>(query);

            for (int i = 0; i < result.Count; i++)
                result[i].SetName($"Customer No {i + 1}");

            Db.BeginTransaction();

            await Db.SaveOrUpdateBatchAsync(result);

            Db.Commit();

            result = await Db.ToListAsync<Customers>(query);

            Assert.Equal(2, result.Count());
            Assert.Equal("Customer No 1", result.First().Name);
            Assert.Equal("Customer No 2", result.Skip(1).First().Name);
        }

        [Fact]
        public async Task T08_Delete()
        {
            var customerId = _bag.GetValue<int>("Customer1Id");

            Db.BeginTransaction();

            await Db.RemoveAsync<Customers>(customerId);

            Db.Commit();

            Customers customer = null;

            var query = Db.SelectAll(() => customer)
                .OrderBy(() => customer.Name)
                ;

            var result = await Db.ToListAsync<Customers>(query);

            Assert.Equal(1, result.Count());
            Assert.Equal("Customer No 2", result.First().Name);
        }

        [Fact]
        public async Task T09_TransactionRolledBack()
        {
            Customers customer = null;

            var query = Db.SelectAll(() => customer)
                .OrderBy(() => customer.Name)
                ;

            var result = await Db.ToListAsync<Customers>(query);

            Db.BeginTransaction();

            await Db.RemoveAsync<Customers>(result.First().Id);

            Db.Rollback();

            result = await Db.ToListAsync<Customers>(query);

            Assert.Equal(1, result.Count());
            Assert.Equal("Customer No 2", result.First().Name);
        }

        [Fact]
        public async Task T10_NoTransaction()
        {
            Customers customer = null;

            var query = Db.SelectAll(() => customer)
                .OrderBy(() => customer.Name)
                ;

            var result = await Db.ToListAsync<Customers>(query);

            await Db.RemoveAsync<Customers>(result.First().Id);

            result = await Db.ToListAsync<Customers>(query);

            Assert.Equal(0, result.Count());
        }

        [Fact]
        public async Task T11_Paginated()
        {
            var customers = new[]
            {
                new Customers("Customer 1"),
                new Customers("Customer 2"),
                new Customers("Customer 3"),
                new Customers("Customer 4"),
                new Customers("Customer 5"),
                new Customers("Customer 6"),
                new Customers("Customer 7"),
                new Customers("Customer 8"),
                new Customers("Customer 9"),
                new Customers("Customer 10"),
                new Customers("Customer 11"),
            };

            await Db.SaveOrUpdateBatchAsync(customers);

            Customers customer = null;

            var query = Db.SelectAll(() => customer)
                .OrderBy(() => customer.Name)
                ;

            var result = await Db.ToPaginatedAsync<Customers>(query, 1, 5);

            Assert.Equal(5, result.List.Count());
            Assert.Equal(1, result.Page);
            Assert.Equal(5, result.PerPage);
            Assert.Equal(11, result.Count);
            Assert.Equal(3, result.TotalPages);

            result = await Db.ToPaginatedAsync<Customers>(query, 2, 5);

            Assert.Equal(5, result.List.Count());
            Assert.Equal(2, result.Page);
            Assert.Equal(5, result.PerPage);
            Assert.Equal(11, result.Count);
            Assert.Equal(3, result.TotalPages);

            result = await Db.ToPaginatedAsync<Customers>(query, 3, 5);

            Assert.Equal(1, result.List.Count());
            Assert.Equal(3, result.Page);
            Assert.Equal(5, result.PerPage);
            Assert.Equal(11, result.Count);
            Assert.Equal(3, result.TotalPages);

            result = await Db.ToPaginatedAsync<Customers>(query, 2, 3);

            Assert.Equal(3, result.List.Count());
            Assert.Equal(2, result.Page);
            Assert.Equal(3, result.PerPage);
            Assert.Equal(11, result.Count);
            Assert.Equal(4, result.TotalPages);
        }

    }
}
