﻿using QueryMan.Tests.Entities.SnakeCase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QueryMan.Tests.AllTests
{
    public class QueryHelperTests
    {
        [Fact]
        public void IsEnumerable()
        {
            Assert.True(QueryHelper.IsEnumerable<Customers[]>());
            Assert.True(QueryHelper.IsEnumerable<IEnumerable<Customers>>());
            Assert.True(QueryHelper.IsEnumerable<IQueryable<Customers>>());
            Assert.True(QueryHelper.IsEnumerable<IList<Customers>>());
            Assert.True(QueryHelper.IsEnumerable<List<Customers>>());

            Assert.False(QueryHelper.IsEnumerable<Customers>());
        }
    }
}
