using QueryMan.Tests.Base;
using QueryMan.Tests.Entities.SnakeCase;
using SqlKata.Compilers;
using Xunit;

namespace QueryMan.Tests.AllTests.QueryBuilderTests
{
    public class SnakeCaseTests : BaseTest
    {
        public SnakeCaseTests()
        {
        }

        [Fact]
        public void Basic()
        {
            Customers customer = null;
            (string CustomerId, string CustomerName) result = default;

            var query = GetQueryBuilder(true)
                .From(() => customer)
                .Select(() => customer.Id, () => result.CustomerId)
                .Select(() => customer.Name, () => result.CustomerName)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [customer].[id] AS [Item1], [customer].[name] AS [Item2] FROM [customers] AS [customer]", query_str);
        }

        [Fact]
        public void Where()
        {
            Customers customer = null;
            (string CustomerId, string CustomerName) result = default;

            var query = GetQueryBuilder(true)
                .From(() => customer)
                .Select(() => customer.Id, () => result.CustomerId)
                .Select(() => customer.Name, () => result.CustomerName)
                .Where(() => customer.Id, "ABC")
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [customer].[id] AS [Item1], [customer].[name] AS [Item2] FROM [customers] AS [customer] WHERE [customer].[id] = 'ABC'", query_str);
        }

        [Fact]
        public void Join()
        {
            Contact contact = null;
            Customers customer = null;
            (string FirstName, string LastName, string CustomerId) result = default;

            var query = GetQueryBuilder(true)
                .From(() => contact)
                .Join(() => customer, () => customer.Id, () => contact.CustomerId)
                .Select(() => contact.FirstName, () => result.FirstName)
                .Select(() => contact.LastName, () => result.LastName)
                .Select(() => customer.Id, () => result.CustomerId)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [contact].[first_name] AS [Item1], [contact].[last_name] AS [Item2], [customer].[id] AS [Item3] FROM [contacts] AS [contact] \nINNER JOIN [customers] AS [customer] ON [customer].[id] = [contact].[customer_id]", query_str);
        }

        [Fact]
        public void JoinBuilder()
        {
            Contact contact = null;
            Customers customer = null;
            (string FirstName, string LastName, string CustomerId) result = default;

            var query = GetQueryBuilder(true)
                .From(() => contact)
                .Join(() => customer, (join) => join.WhereColumns(() => customer.Id, () => contact.CustomerId))
                .Select(() => contact.FirstName, () => result.FirstName)
                .Select(() => contact.LastName, () => result.LastName)
                .Select(() => customer.Id, () => result.CustomerId)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [contact].[first_name] AS [Item1], [contact].[last_name] AS [Item2], [customer].[id] AS [Item3] FROM [contacts] AS [contact] \nINNER JOIN [customers] AS [customer] ON ([customer].[id] = [contact].[customer_id])", query_str);
        }

        [Fact]
        public void GroupBy()
        {
            Customers customer = null;
            Contact contact = null;

            (string CustomerName, int ContactCount) result = default;

            var query = GetQueryBuilder(true)
                .From(() => contact)
                .Join(() => customer, (join) => join.WhereColumns(() => customer.Id, () => contact.CustomerId))
                .Select(() => customer.Name, () => result.CustomerName)
                .SelectCount(() => contact.Id, () => result.ContactCount)
                .GroupBy()
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [customer].[name] AS [Item1], COUNT(contact.id) AS Item2 FROM [contacts] AS [contact] \nINNER JOIN [customers] AS [customer] ON ([customer].[id] = [contact].[customer_id]) GROUP BY [customer].[name]", query_str);
        }

        [Fact]
        public void OrderBy()
        {
            Customers customer = null;

            var query = GetQueryBuilder(true)
                .SelectAll(() => customer)
                .OrderBy(() => customer.Name)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT name AS Name, id AS Id FROM [customers] AS [customer] ORDER BY [customer].[name]", query_str);
        }

        [Fact]
        public void SkipTake()
        {
            Customers customer = null;

            var query = GetQueryBuilder(true)
                .SelectAll(() => customer)
                .Skip(10)
                .Take(20)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT * FROM (SELECT name AS Name, id AS Id, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS [row_num] FROM [customers] AS [customer]) AS [results_wrapper] WHERE [row_num] BETWEEN 11 AND 30", query_str);
        }

        [Fact]
        public void PerPage()
        {
            Customers customer = null;

            var query = GetQueryBuilder(true)
                .SelectAll(() => customer)
                .ForPage(2, 10)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT * FROM (SELECT name AS Name, id AS Id, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS [row_num] FROM [customers] AS [customer]) AS [results_wrapper] WHERE [row_num] BETWEEN 11 AND 20", query_str);
        }

    }
}