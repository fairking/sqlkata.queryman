using QueryMan.Tests.Base;
using QueryMan.Tests.Entities.PascalCase;
using SqlKata.Compilers;
using Xunit;

namespace QueryMan.Tests.AllTests.QueryBuilderTests
{
    public class PascalCaseTests : BaseTest
    {
        public PascalCaseTests()
        {
        }

        [Fact]
        public void Basic()
        {
            Customers customer = null;
            (string CustomerId, string CustomerName) result = default;

            var query = GetQueryBuilder(false)
                .From(() => customer)
                .Select(() => customer.Id, () => result.CustomerId)
                .Select(() => customer.Name, () => result.CustomerName)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [customer].[Id] AS [Item1], [customer].[Name] AS [Item2] FROM [Customers] AS [customer]", query_str);
        }

        [Fact]
        public void Where()
        {
            Customers customer = null;
            (string CustomerId, string CustomerName) result = default;

            var query = GetQueryBuilder(false)
                .From(() => customer)
                .Select(() => customer.Id, () => result.CustomerId)
                .Select(() => customer.Name, () => result.CustomerName)
                .Where(() => customer.Id, "ABC")
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [customer].[Id] AS [Item1], [customer].[Name] AS [Item2] FROM [Customers] AS [customer] WHERE [customer].[Id] = 'ABC'", query_str);
        }

        [Fact]
        public void Join()
        {
            Contact contact = null;
            Customers customer = null;
            (string FirstName, string LastName, string CustomerId) result = default;

            var query = GetQueryBuilder(false)
                .From(() => contact)
                .Join(() => customer, () => customer.Id, () => contact.CustomerId)
                .Select(() => contact.FirstName, () => result.FirstName)
                .Select(() => contact.LastName, () => result.LastName)
                .Select(() => customer.Id, () => result.CustomerId)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [contact].[FirstName] AS [Item1], [contact].[LastName] AS [Item2], [customer].[Id] AS [Item3] FROM [Contacts] AS [contact] \nINNER JOIN [Customers] AS [customer] ON [customer].[Id] = [contact].[CustomerId]", query_str);
        }

        [Fact]
        public void JoinBuilder()
        {
            Contact contact = null;
            Customers customer = null;
            (string FirstName, string LastName, string CustomerId) result = default;

            var query = GetQueryBuilder(false)
                .From(() => contact)
                .Join(() => customer, (join) => join.WhereColumns(() => customer.Id, () => contact.CustomerId))
                .Select(() => contact.FirstName, () => result.FirstName)
                .Select(() => contact.LastName, () => result.LastName)
                .Select(() => customer.Id, () => result.CustomerId)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [contact].[FirstName] AS [Item1], [contact].[LastName] AS [Item2], [customer].[Id] AS [Item3] FROM [Contacts] AS [contact] \nINNER JOIN [Customers] AS [customer] ON ([customer].[Id] = [contact].[CustomerId])", query_str);
        }

        [Fact]
        public void GroupBy()
        {
            Customers customer = null;
            Contact contact = null;

            (string CustomerName, int ContactCount) result = default;

            var query = GetQueryBuilder(false)
                .From(() => contact)
                .Join(() => customer, (join) => join.WhereColumns(() => customer.Id, () => contact.CustomerId))
                .Select(() => customer.Name, () => result.CustomerName)
                .SelectCount(() => contact.Id, () => result.ContactCount)
                .GroupBy()
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT [customer].[Name] AS [Item1], COUNT(contact.Id) AS Item2 FROM [Contacts] AS [contact] \nINNER JOIN [Customers] AS [customer] ON ([customer].[Id] = [contact].[CustomerId]) GROUP BY [customer].[Name]", query_str);
        }

        [Fact]
        public void OrderBy()
        {
            Customers customer = null;

            var query = GetQueryBuilder(false)
                .SelectAll(() => customer)
                .OrderBy(() => customer.Name)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT Name AS Name, Id AS Id FROM [Customers] AS [customer] ORDER BY [customer].[Name]", query_str);
        }

        [Fact]
        public void SkipTake()
        {
            Customers customer = null;

            var query = GetQueryBuilder(false)
                .SelectAll(() => customer)
                .Skip(10)
                .Take(20)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT * FROM (SELECT Name AS Name, Id AS Id, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS [row_num] FROM [Customers] AS [customer]) AS [results_wrapper] WHERE [row_num] BETWEEN 11 AND 30", query_str);
        }

        [Fact]
        public void PerPage()
        {
            Customers customer = null;

            var query = GetQueryBuilder(false)
                .SelectAll(() => customer)
                .ForPage(2, 10)
                .ToQuery();

            var query_str = new SqlServerCompiler().Compile(query).ToString();

            Assert.NotNull(query_str);
            Assert.Equal("SELECT * FROM (SELECT Name AS Name, Id AS Id, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS [row_num] FROM [Customers] AS [customer]) AS [results_wrapper] WHERE [row_num] BETWEEN 11 AND 20", query_str);
        }

    }
}